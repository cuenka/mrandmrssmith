<?php

namespace App\Tests;

use App\Entity\Calculator;
use App\Service\OperatorFactory;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class OperatorPlusTest
 */
class OperatorPlusTest extends WebTestCase
{
    /**
     * @test testResolve
     */
    public function testResolve()
    {

        $operator =  OperatorFactory::create(Calculator::PLUS);

        $this->assertEquals(
            6,
            $operator->resolve(2, 4)
        );
    }
}
