<?php

namespace App\Service;

use App\Entity\Calculator;
use PHPUnit\Framework\TestCase;

/**
 * Class OperatorDivisionTest
 * @package App\Service
 */
class OperatorDivisionTest extends TestCase
{

    /**
     * @test testResolve
     */
    public function testResolve()
    {

        $operator =  OperatorFactory::create(Calculator::DIVISION);

        $this->assertEquals(
            2,
            $operator->resolve(6, 3)
        );
    }
}
