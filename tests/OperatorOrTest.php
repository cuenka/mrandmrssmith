<?php

namespace App\Service;

use App\Entity\Calculator;
use PHPUnit\Framework\TestCase;

/**
 * Class OperatorOrTest
 * @package App\Service
 */
class OperatorOrTest extends TestCase
{
    /**
     * @test testResolve
     */
    public function testResolve()
    {

        $operator =  OperatorFactory::create(Calculator::BITWISEOR);

        $this->assertEquals(
            1,
            $operator->resolve(1, 0)
        );
    }
}
