<?php

namespace App\Tests;

use App\Entity\Calculator;
use App\Service\OperatorFactory;
use App\Service\OperatorMinus;
use PHPUnit\Framework\TestCase;

/**
 * Class OperatorMinusTest
 * @package App\Tests
 */
class OperatorMinusTest extends TestCase
{
    /**
     * @test testResolve
     */
    public function testResolve()
    {
        $operator =  OperatorFactory::create(Calculator::MINUS);

        $this->assertEquals(
            6,
            $operator->resolve(12, 6)
        );
    }
}
