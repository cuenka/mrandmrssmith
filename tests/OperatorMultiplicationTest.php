<?php

namespace App\Tests;

use App\Entity\Calculator;
use App\Service\OperatorFactory;
use App\Service\OperatorMultiplication;
use PHPUnit\Framework\TestCase;

/**
 * Class OperatorMultiplicationTest
 * @package App\Tests
 */
class OperatorMultiplicationTest extends TestCase
{
    /**
     * @test testResolve
     */
    public function testResolve()
    {

        $operator =  OperatorFactory::create(Calculator::MULTIPLICATION);

        $this->assertEquals(
            6,
            $operator->resolve(2, 3)
        );
    }
}
