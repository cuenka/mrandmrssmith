<?php

namespace App\Service;

use App\Entity\Calculator;
use PHPUnit\Framework\TestCase;

/**
 * Class OperatorAndTest
 * @package App\Service
 */
class OperatorAndTest extends TestCase
{
    /**
     * @test testResolve
     */
    public function testResolve()
    {

        $operator =  OperatorFactory::create(Calculator::BITWISEAND);

        $this->assertEquals(
            0,
            $operator->resolve(1, 0)
        );
    }
}
