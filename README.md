# README #

This README contains useful information for installing repository

### What is this repository for? ###
This is for mrandmrssmith staff

### How do I get set up? ###

Download repository and execute folowing comands:
```
php composer.phar install

yarn install
yarn add @symfony/webpack-encore --dev
yarn add sass-loader@^7.0.1 node-sass --dev
yarn add bootstrap
yarn add popper.js
yarn add jquery
yarn encore dev
```
=====================

It contain 6 unit tests.
Running unit test.
```
php bin/phpunit
```
Any question, feel free to ask. 

Jose Cuenca