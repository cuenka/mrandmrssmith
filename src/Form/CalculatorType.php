<?php

namespace App\Form;

use App\Entity\Calculator;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class CalculatorType
 * @package App\Form
 */
class CalculatorType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('operation', ChoiceType::class, [
                'required' => true,
                'choices' => [
                    'Plus'           => Calculator::PLUS,
                    'Minus'          => Calculator::MINUS,
                    'Multiplication' => Calculator::MULTIPLICATION,
                    'Division'       => Calculator::DIVISION,
                    'And'            => Calculator::BITWISEAND,
                    'Or'             => Calculator::BITWISEOR,
                ],
                'attr' => [
                    'class' => 'mt-1',
                    'placeholder' => 'Select Operation',
                ],
            ])
            ->add('firstNumber', IntegerType::class, [
                'required' => true,
                'attr' => [
                    'class' => 'mt-1',
                    'placeholder' => 'Introduce first number',
                ],
            ])
            ->add('secondNumber', IntegerType::class, [
                'required' => true,
                'attr' => [
                    'class' => 'mt-1',
                    'placeholder' => 'Introduce first number',
                ],
            ])
            ->add('Calculate', SubmitType::class, [
                'label' => 'Calculate',
                    'attr' => [
                        'class' => 'btn btn-success mt-1',
                    ],
                ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Calculator::class,
        ]);
    }
}
