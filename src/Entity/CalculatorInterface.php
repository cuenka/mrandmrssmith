<?php

namespace App\Entity;

/**
 * Interface CalculatorInterface
 * @package App\Entity
 */
interface CalculatorInterface
{
    /**
     * It will return the value of firstNumber, depending of class would be integer or float
     * @return mixed
     */
    public function getFirstNumber();

    /**
     * It will return the value of firstNumber, depending of class would be integer or float
     * @return mixed
     */
    public function getsecondNumber();

    /**
     * Returns type of operation
     * @return string
     */
    public function getOperation();
}
