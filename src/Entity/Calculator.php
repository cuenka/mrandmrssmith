<?php

namespace App\Entity;


/**
 * Class Calculator
 * @package App\Entity
 */
class Calculator implements CalculatorInterface
{
    const PLUS = 'plus';
    const MINUS = 'minus';
    const MULTIPLICATION = 'multiplication';
    const DIVISION = 'division';
    const BITWISEAND = 'and';
    const BITWISEOR = 'or';

    /**
     * @var integer $firstNumber
     */
    private $firstNumber;

    /**
     * @var integer $secondNumber
     */
    private $secondNumber;

    /**
     * @var integer $operation
     */
    private $operation;

    /**
     * @return integer
     */
    public function getFirstNumber(): ?int
    {
        return $this->firstNumber;
    }

    /**
     * @param integer $firstNumber
     * @return Calculator
     */
    public function setFirstNumber(int $firstNumber): self
    {
        $this->firstNumber = $firstNumber;

        return $this;
    }

    /**
     * @return integer
     */
    public function getSecondNumber(): ?int
    {
        return $this->secondNumber;
    }

    /**
     * @param integer $secondNumber
     * @return Calculator
     */
    public function setSecondNumber(int $secondNumber): self
    {
        $this->secondNumber = $secondNumber;

        return $this;
    }

    /**
     * @return string
     */
    public function getOperation(): ?string
    {
        return $this->operation;
    }

    /**
     * @param string $operation
     * @return Calculator
     */
    public function setOperation(string $operation): self
    {
        $this->operation = $operation;

        return $this;
    }
}
