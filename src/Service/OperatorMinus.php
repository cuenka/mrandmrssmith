<?php

namespace App\Service;

/**
 * Class OperatorMinus
 * @package App\Service
 */
class OperatorMinus extends Operator implements OperatorInterface
{
    /**
     * @param integer $firstNumber
     * @param integer $secondNumber
     * @return integer
     */
    public function resolve(int $firstNumber, int $secondNumber) : ? int
    {
        $result = intval($firstNumber - $secondNumber);

        return $result;
    }
}