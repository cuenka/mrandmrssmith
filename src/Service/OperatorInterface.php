<?php
/**
 * Created by PhpStorm.
 * User: jose
 * Date: 27/02/19
 * Time: 21:23
 */

namespace App\Service;


interface OperatorInterface
{
    /**
     * @param integer $firstNumber
     * @param integer $secondNumber
     * @return mixed
     */
    public function resolve(int $firstNumber, int $secondNumber);
}
