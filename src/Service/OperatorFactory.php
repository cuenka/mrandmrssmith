<?php

namespace App\Service;

use App\Entity\Calculator;

/**
 * Class OperatorFactory
 * @package App\Service
 */
class OperatorFactory
{
    /**
     * I applied SOLID Principle and Factory method pattern
     * @param string $operation
     * @return OperatorInterface|null
     */
    public static function create(string $operation): ?OperatorInterface
    {
        switch ($operation) {
            case Calculator::PLUS:
                return new OperatorPlus();
                break;
            case Calculator::MINUS:
                return new OperatorMinus();
                break;
            case Calculator::MULTIPLICATION:
                return new OperatorMultiplication();
                break;
            case Calculator::DIVISION:
                return new OperatorDivision();
                break;
            case Calculator::BITWISEAND:
                return new OperatorAnd();
                break;
            case Calculator::BITWISEOR:
                return new OperatorOr();
        }
    }
}


