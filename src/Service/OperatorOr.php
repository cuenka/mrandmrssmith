<?php

namespace App\Service;

/**
 * Class OperatorOr
 * @package App\Service
 */
class OperatorOr extends Operator implements OperatorInterface
{
    /**
     * @param integer $firstNumber
     * @param integer $secondNumber
     * @return float
     */
    public function resolve(int $firstNumber, int $secondNumber) : ? float
    {
        $result = ($firstNumber | $secondNumber);

        return $result;
    }
}
