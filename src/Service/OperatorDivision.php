<?php

namespace App\Service;

/**
 * Class OperatorDivision
 * @package App\Service
 */
class OperatorDivision extends Operator implements OperatorInterface
{
    /**
     * @param integer $firstNumber
     * @param integer $secondNumber
     * @return float
     */
    public function resolve(int $firstNumber, int $secondNumber) : ? float
    {
        if ($this->validation($firstNumber, $secondNumber) === false) {
            return null;
        }

        $result = floatval($firstNumber / $secondNumber);

        return $result;
    }

    /**
     * @param integer $firstNumber
     * @param integer $secondNumber
     * @return boolean
     */
    private function validation(int $firstNumber, int $secondNumber) : ? bool
    {
        // this is infinity
        if ($firstNumber !=0 && $secondNumber == 0) {
            return false;
        }
        // this is error
        if ($firstNumber ==0 && $secondNumber == 0) {
            return false;
        }

        return true;
    }
}