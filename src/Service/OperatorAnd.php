<?php

namespace App\Service;

/**
 * Class OperatorAnd
 * @package App\Service
 */
class OperatorAnd extends Operator implements OperatorInterface
{
    /**
     * @param integer $firstNumber
     * @param integer $secondNumber
     * @return float
     */
    public function resolve(int $firstNumber, int $secondNumber) : ? float
    {
        $result = ($firstNumber & $secondNumber);

        return $result;
    }
}
