<?php

namespace App\Service;

/**
 * Class Operator
 * @package App\Service
 */
abstract class Operator
{
    /**
     * @param integer $firstNumber
     * @param integer $secondNumber
     * @return mixed
     */
    abstract public function resolve(int $firstNumber, int $secondNumber);
}
