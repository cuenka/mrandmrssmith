<?php

namespace App\Controller;

use App\Entity\Calculator;
use App\Form\CalculatorType;
use App\Service\OperatorFactory;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class CalculatorController
 * @package App\Controller
 */
class CalculatorController extends AbstractController
{
    /**
     * @Route("/", name="calculator_index")
     */
    public function index(Request $request)
    {
        $calculator = new Calculator();
        $result = null;

        $form = $this->createForm(CalculatorType::class, $calculator);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $formRequest = $request->request->all();
            // Service is autowired
            $operator = OperatorFactory::create($formRequest['calculator']['operation']);

            $result = $operator->resolve(
                $formRequest['calculator']['firstNumber'],
                $formRequest['calculator']['secondNumber']
            );

            $this->addFlash('success', '<strong>Excellent!</strong> Calculator submitted');
        }


        return $this->render('calculator/index.html.twig', [
            'controller_name' => 'CalculatorController',
            'form' => $form->createView(),
            'result' => $result,
        ]);
    }
}
